#! /bin/sh


# User credentials
user=admin
email=admin@example.com
password=pass

file=db/db.sqlite3

#if [ -z "$file" ]; then
#  echo "from django.contrib.auth.models import User; User.objects.create_superuser('$user', '$email', '$password')" | python3 manage.py shell
#fi
python3 manage.py migrate
# добавлен оператор || , который позволяет использовать docker volume, так как при повторном запуске контейнера 
# отрабатывает команда создания пользователя admin, что выдаст ошибку, потому что пользователь уже существует в БД
echo "from django.contrib.auth.models import User; User.objects.create_superuser('$user', '$email', '$password')" | python3 manage.py shell || true
